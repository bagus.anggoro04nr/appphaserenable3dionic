import { Component } from '@angular/core';
import 'Phaser';
// import { Scene3D, Project } from 'enable3d';
import { Scene3D, enable3d, Canvas, PhysicsLoader,
  ExtendedObject3D, THREE, ExtendedMesh } from '@enable3d/phaser-extension';
import { Physics, Ammo } from '@enable3d/ammo-on-nodejs';


@Component({
  selector: 'app-lvls',
  templateUrl: './lvls.page.html',
  styleUrls: ['./lvls.page.scss'],
})
export class LvlsPage extends Scene3D {

  public background: any;
  public targetWidth: any;
  public targetHeight: any;
  public boxScale: any;
  public boxShape: any;
  public boxShapeAnother: any;
  public boxImg: any;
  public boxImgAnother: any;
  public boxExtr: any;
  public boxImgs: any[] = [];
  public boxPositions: any;
  public graphics: any;

  public posScreen: any[] = [];
  private posX: any;
  private posY: any;
  private rowCol: any;
  public location: Location;

  public colRow: any[] = [];

  public pictPosition: any;
  public pickedObject: any;
  public movePosition: any;

  public shapes1: any;
  public prom: any;

  constructor() {
    super({ key: 'LvlsPage'});

    this.targetWidth = window.outerWidth;
    this.targetHeight = window.outerHeight;


  }

  init() {
     this.accessThirdDimension()
     delete this.boxShape
     this.third.renderer.setPixelRatio( window.devicePixelRatio );
     this.third.renderer.setSize( window.innerWidth, window.innerHeight );
  }

  preload () {
        // console.log(this);
        // this.renderer.domElement.addEventListener("mouseover", this.onOver.bind(this, { passive: true} ), true);
        // this.renderer.domElement.addEventListener("mousemove", this.onMove.bind(this, { passive: true} ), true);

        // this.third.load.preload('allbutton', '../../../assets/img/lvl1/allbutton.svg');
        this.third.load.preload('background', '../../../assets/img/bg/forest.png');
        // this.third.load.preload('box', '../../../assets/img/lvl1/button_netral.svg');
  }

  create() {

    this.third.warpSpeed( 'light', 'camera', '-sky', '-orbitControls', '-ground')
    this.third.camera.position.set(0, 0, 20)


    // add platforms
   const platformMaterial = { phong: { transparent: true, opacity: 0 } }
   // const platforms = [
   //   this.third.physics.add.box({ name: 'platform-ground', x: -8, y: -8, width: 30, depth: 2, height: 1, mass: 0}, platformMaterial),
     // this.third.physics.add.box({ name: 'platform-right1', x: 7, y: 4, width: 15, depth: 5, mass: 0 }, platformMaterial),
     // this.third.physics.add.box({ name: 'platform-left', x: -10, y: 7, width: 10, depth: 5, mass: 0 }, platformMaterial),
     // this.third.physics.add.box({ name: 'platform-right2', x: 10, y: 10, width: 10, depth: 5, mass: 0 }, platformMaterial)
   // ]

    this.third.load.texture('background').then(background => (this.third.scene.background = background));
    this.boxPositions = this.centerOfScreen(9);
    // add a sensor
    const sensor = new ExtendedObject3D()
    // sensor.position.setY(0)
    this.third.physics.add.existing(sensor, { mass: 1e-8, shape: 'box', width: 0.2, height: 0.2, depth: 0.2 })
    sensor.body.setCollisionFlags(4)

    const scalenya = 1;

    this.boxPositions.forEach((pos, ib) => {
      let posXs = JSON.parse(pos);


    this.third.load.gltf('../../../assets/img/lvl1/panda.glb').then(gltf => {

            this.boxShape = new ExtendedObject3D()
            this.boxShape.add(gltf.scene)
            // console.log(gltf)
            this.boxShape.name = `this.boxShape-${ib}`
            this.boxShape.scale.set(scalenya, scalenya, scalenya)

            this.boxShape.traverse(child => {

              if (child.isMesh) {
                child.castShadow = child.receiveShadow = true;

              }
            })

            // animations
            this.third.animationMixers.add(this.boxShape.animation.mixer)
            gltf.animations.forEach(animation => {
              this.boxShape.animation.add(animation.name, animation)
            })
            this.boxShape.animation.play('Rotation')

            this.boxShape.position.setX(posXs.x + 1)
            this.boxShape.position.setY(posXs.y + 2)
            // console.log(posXs.x + 1, posXs.y + 2)
            let platforms =
            [
              this.third.physics.add.box({ name: 'platform-ground', x: posXs.x + 1, y: posXs.y + 2, width: 2, depth: 1, height: 0, mass: 0}, platformMaterial)
            ]


            this.third.add.existing(this.boxShape)

            this.third.physics.add.existing(this.boxShape, { shape: 'box', depth: 120})
            this.boxShape.body.setCollisionFlags(6)
            this.boxShape.body.setLinearFactor(0, 0, 0)
            this.boxShape.body.setAngularFactor(0, 0, 0)
            this.boxShape.body.setFriction(0)


            // connect sensor to boxShape
            this.third.physics.add.constraints.lock(this.boxShape.body, sensor.body)

            sensor.body.on.collision((otherObject, event) => {
              if (/platform/.test(otherObject.name)) {
                if (event !== 'end')
                this.boxShape.userData.onGround = true
                else this.boxShape.userData.onGround = false
              }
            })

            this.boxImgs.push(this.boxShape);


         })

        })
         // this.third.camera.lookAt(this.boxShape.scene.position)
         this.third.camera.lookAt(0, 0, 0)
         // console.log(this.boxImgs);
         // Click or Action
         // this.third.renderer.domElement.ownerDocument.activeElement.addEventListener("click", this.onClick.bind(this));
         this.input.on('pointerdown', event => {
            // var touchX = pointer.x;
            // var touchY = pointer.y;
            this.onClick(event);
         });


         // this.third.scene.scale.set(scalenya, scalenya, scalenya)
         // this.third.camera.scale.set(scalenya, scalenya, scalenya)
         this.third.renderer.render(this.third.scene, this.third.camera);

      // this.tweens.add({
      //     targets: this.logo,
      //     y: 350,
      //     duration: 1500,
      //     ease: 'Sine.inOut',
      //     yoyo: true,
      //     repeat: -1
      // })
  }

  update() {

    if (this.pictPosition !== undefined && this.pictPosition !== '') {

      this.boxImgs.forEach((boxInPic) => {
            // console.log(boxInPic)
            console.log(this.pictPosition.x, this.pictPosition.y);

           if (boxInPic.position.x == this.pictPosition.x && boxInPic.position.y == this.pictPosition.y ) {
             console.log(boxInPic);

                // var cloned = boxInPic.material.clone() // ini di gunakan buat ganti warna hanya satu object
                boxInPic.color.set(0x7cfc00);
                // boxInPic.material = cloned;
                // boxInPic.geometry.dispose();
                // boxInPic.material.dispose();
                // this.scene.remove( boxInPic );
                // this.addObjectAnother(this.pictPosition.x, this.pictPosition.y, cloned);


           }
      });

      this.pictPosition = '';
    }

  }

  onClick(e) {
    let pickPosition = {x: 0, y: 0};
    let pickPosition1 = {x: 0, y: 0};
    this.pictPosition = {x: 0, y: 0};
    var raycaster = new THREE.Raycaster();
    // console.log('ptr:', e)
    // const pos = this.getCanvasRelativePosition(e);
    // console.log('pos: ', pos.x, pos.y)
    console.log(e.x, e.y, e.position.x, e.position.y)
    pickPosition.x = (e.position.x / window.innerWidth ) *  2 - 1;
    pickPosition.y = (e.position.y / window.innerHeight) * -2 + 1;  // note we flip Y
    // var vector = new THREE.Vector3()
    // vector.set(pickPosition.x, pickPosition.y, 1);
    // vector.unproject(this.third.camera);
    // var direction = vector.sub(this.third.camera.position).normalize(),
    //   distance = - this.third.camera.position.z / direction.z,
    //   scaled = direction.multiplyScalar(distance),
    //   coords = this.third.camera.position.clone().add(scaled);
    pickPosition1.x = pickPosition.x;
    pickPosition1.y = pickPosition.y;
    console.log('uyee: ', pickPosition.x, pickPosition.y)
    raycaster.setFromCamera(pickPosition1, this.third.camera);
    // console.log(this.third.scene.children);
    const intersectedObjects = raycaster.intersectObjects(this.third.scene.children);

    if (intersectedObjects.length) {

      this.pickedObject = intersectedObjects[0].object;

      if (this.pickedObject.name.length > 0) console.log(this.pickedObject.name);
      this.pictPosition.x = this.pickedObject.position.x
      this.pictPosition.y = this.pickedObject.position.y


       // console.log('bobo: ', this.pictPosition.x, this.pictPosition.y, this.pickedObject);
   }

  };

  getCanvasRelativePosition(ptrs) {
    const rect = this.third.renderer.domElement.ownerDocument.activeElement.getBoundingClientRect();
    console.log(rect)
    return {
      x:  (ptrs.clientX - rect.left) * (window.innerWidth / rect.width) ,
      y:  (ptrs.clientY - rect.top ) * (window.innerHeight / rect.height),
    };
  }


   centerOfScreen(data) {

     this.rowCol = this.numrow(data);
     // console.log(this.rowCol);
     this.posX = 0;
     this.posY = 2.5 * this.rowCol.row;

     for(let b = 0; b < data; b++) {

       for(let i=0; i < this.rowCol.row; i++) {

           if ( Number.isInteger(b/this.rowCol.col) === true) {

             this.posY -= 1.75;
             this.posX = 2 * this.rowCol.row;
           }


           for(let a = 0; a < this.rowCol.col; a++) {


             if ( Number.isInteger(a / this.rowCol.col) === true) this.posX -= 2;
             // console.log('x : ' + this.posX);
             this.posScreen[b] = '{"x":' + this.posX + ', "y":' + this.posY + '}';

           }

       }

     }
     // console.log(JSON.parse(JSON.stringify(this.posScreen)));
     return this.posScreen;

   }

   numrow(number) {
     let baris = 0;
     let a = 1;
     let column = 0;
     for(a = 1; a <= number; a++){

     if(Math.floor(number / a) >= a){

           baris = a;
           column = (number/a);

       }continue;

     }

     this.colRow['row'] = baris;
     this.colRow['col'] = column;

     return this.colRow;
   }

  }

  const config = {
    type: Phaser.WEBGL,
    parent: 'content',
    transparent: true,
    antialias: false,
    // width: window.outerWidth,
    // height: window.outerHeight,
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH,
        width: window.innerWidth ,
        height: window.innerHeight ,
        pageAlignHorizontally: true,
        pageAlignVertically: true,
    },
    scene: [ LvlsPage ],
    ...Canvas()
  };

  // const config = {
  //         type: Phaser.WEBGL,
  //         transparent: true,
  //         scale: {
  //           mode: Phaser.Scale.FIT,
  //           autoCenter: Phaser.Scale.CENTER_BOTH,
  //           width: window.innerWidth * Math.max(1, window.devicePixelRatio / 2),
  //           height: window.innerHeight * Math.max(1, window.devicePixelRatio / 2)
  //         },
  //         scenes: [MainLvl1Component],
  //         ...Canvas()
  //       }

  const wasm = '../../../assets/lib';
  window.addEventListener('load', () => {
  // PhysicsLoader('../../assets/lib/', () => new Phaser.Game(config));
    enable3d(() => new Phaser.Game(config)).withPhysics(wasm);
  });
