import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'lvls',
    pathMatch: 'full'
  },
  {
    path: 'mainstart',
    loadChildren: () => import('./pages/mainstart/mainstart.module').then( m => m.MainstartPageModule)
  },
  // {
  //   path: 'lvl1',
  //   loadChildren: () => import('./pages/lvl1/lvl1.module').then( m => m.Lvl1PageModule)
  // },
  {
    path: 'lvls',
    loadChildren: () => import('./pages/lvls/lvls.module').then( m => m.LvlsPageModule)
  },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
